package com.jkorah.atm;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Scanner;

public class ATMRunner {

    private static BankAccount currentAccount = null;

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        addTestBankAccounts();
        boolean keepRunning = true;

        while (keepRunning){
            System.out.println("\nPlease choose an Option Below \n================");
            if (currentAccount == null) {
                presentMainOptions();
                keepRunning = handleMainInput(in);
            } else {
                presentAccountOptions();
                keepRunning = handleAccountInput(in);
            }

        }
    }

    private static void addTestBankAccounts() {
        BankAccount account1 = new BankAccount("1234", "Justin Korah", BigDecimal.ZERO);
        BankAccount account2 = new BankAccount("4321", "Dave Grohl", BigDecimal.TEN);
        try {
            AccountService.getInstance().addAccount(account1);
            AccountService.getInstance().addAccount(account2);
        } catch (AccountTakenException e) {
            e.printStackTrace();
        }
    }

    private static boolean handleMainInput(Scanner in) {
        String command = in.next();
        switch (command){
            case "1":
                System.out.println("Please enter a pin number: ");
                String pin = in.next();
                currentAccount = AccountService.getInstance().findAccount(pin);
                if (currentAccount == null){
                    System.out.println("Could not find the bank account with the given pin number.");
                } else {
                    System.out.printf("Successfully logged into account for %s\n", currentAccount.getCustomerName());
                }
                break;
            case "2":
                return false;
            default:
                System.out.println("Invalid Input!");
                handleMainInput(in);
                break;
        }
        return true;
    }

    private static boolean handleAccountInput(Scanner in) {
        String command = in.next();
        switch (command){
            case "1":
                System.out.printf("Current balance is : $%s\n", currentAccount.getBalance());
                break;
            case "2":
                System.out.printf("Enter a withdrawal amount : ");
                BigDecimal withdrawalAmount = new BigDecimal(in.next().replace("$", ""));
                try {
                    AccountService.getInstance().withdraw(currentAccount,withdrawalAmount);
                } catch (InsufficientFundsException ex){
                    System.out.println("Insufficient funds for the withdrawal");
                }
                break;
            case "3":
                System.out.printf("Enter a deposit amount : ");
                BigDecimal depositAmount = new BigDecimal(in.next().replace("$",""));
                try {
                    AccountService.getInstance().deposit(currentAccount,depositAmount);
                } catch (NegativeDepositException ex) {
                    System.out.println("Please enter a positive amount for deposit.");
                }
                break;
            case "4":
                currentAccount = null;
                break;
            case "5":
                return false;
            default:
                System.out.println("Invalid Input!");
                handleAccountInput(in);
                break;
        }
        return true;
    }

    private static void presentMainOptions() {
        System.out.println("1 - Enter Pin Number");
        System.out.println("2 - Quit");
    }

    private static void presentAccountOptions() {
        System.out.println("1 - View Balance");
        System.out.println("2 - Withdrawal");
        System.out.println("3 - Deposit");
        System.out.println("4 - Logout");
        System.out.println("5 - Quit");
    }
}
