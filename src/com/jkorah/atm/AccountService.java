package com.jkorah.atm;

import java.math.BigDecimal;
import java.util.HashMap;

public class AccountService {

    private static AccountService instance = null;
    private HashMap<String, BankAccount> accountMap = new HashMap<>();
    public BankAccount findAccount(String pin) {
        return accountMap.get(pin);
    }

    public void addAccount(BankAccount account) throws AccountTakenException{
        if (accountMap.containsKey(account.getPin())) {
            throw new AccountTakenException();
        }
        accountMap.put(account.getPin(), account);
    }

    public static AccountService getInstance() {
        if (instance == null){
            instance = new AccountService();
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    public void withdraw(BankAccount account, BigDecimal amount) throws InsufficientFundsException {
        if (account.getBalance().compareTo(amount) < 0){
            throw new InsufficientFundsException();
        }
        account.setBalance(account.getBalance().subtract(amount));

    }

    public void deposit(BankAccount account, BigDecimal depositAmount) throws NegativeDepositException{
        if (depositAmount.compareTo(BigDecimal.TEN) <= 0){
            throw new NegativeDepositException();
        }
        account.setBalance(account.getBalance().add(depositAmount));
    }

}
