package com.jkorah.atm;

import java.math.BigDecimal;

public class BankAccount {

    private String customerName;
    private BigDecimal balance;
    private String pin;

    public BankAccount(String pin, String customerName, BigDecimal balance) {
        this.pin = pin;
        this.customerName = customerName;
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getPin() {
        return pin;
    }

}
