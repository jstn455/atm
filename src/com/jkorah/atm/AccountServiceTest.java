package com.jkorah.atm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class AccountServiceTest {
    AccountService accountService;

    @Before
    public void setUp() throws Exception {
        accountService = AccountService.getInstance();

    }
    @After
    public void tearDown() throws Exception {
        accountService.destroyInstance();
    }

    @Test
    public void testAddAndFindAccount() throws AccountTakenException {
        assertEquals(null, accountService.findAccount("1234"));
        accountService.addAccount(new BankAccount("1234", "Justin Korah", BigDecimal.ZERO));
        BankAccount testAccount = accountService.findAccount("1234");
        assertEquals("1234", testAccount.getPin());
        assertEquals("Justin Korah", testAccount.getCustomerName());
        assertEquals(BigDecimal.ZERO, testAccount.getBalance());
    }

    @Test(expected = AccountTakenException.class)
    public void testAddDuplicateAccount()  throws AccountTakenException{
        assertEquals(null,accountService.findAccount("1234"));
        accountService.addAccount(new BankAccount("1234", "Justin Korah", BigDecimal.ZERO));
        accountService.addAccount(new BankAccount("1234", "Duplicate Account", BigDecimal.ZERO));
    }

    @Test
    public void testWithdrawal() throws AccountTakenException, InsufficientFundsException {
        accountService.addAccount(new BankAccount("1234", "Justin Korah", BigDecimal.valueOf(50)));
        BankAccount testAccount = accountService.findAccount("1234");
        assertEquals(BigDecimal.valueOf(50), testAccount.getBalance());
        accountService.withdraw(testAccount, BigDecimal.valueOf(30));
        assertEquals(BigDecimal.valueOf(20),testAccount.getBalance());
    }

    @Test(expected = InsufficientFundsException.class)
    public void testOverdraftWithdrawal() throws AccountTakenException, InsufficientFundsException {
        accountService.addAccount(new BankAccount("1234", "Justin Korah", BigDecimal.valueOf(50)));
        BankAccount testAccount = accountService.findAccount("1234");
        assertEquals(BigDecimal.valueOf(50), testAccount.getBalance());
        accountService.withdraw(testAccount, BigDecimal.valueOf(60));
    }

    @Test
    public void testDeposit() throws AccountTakenException, NegativeDepositException {
        accountService.addAccount(new BankAccount("1234", "Justin Korah", BigDecimal.valueOf(50)));
        BankAccount testAccount = accountService.findAccount("1234");
        assertEquals(BigDecimal.valueOf(50), testAccount.getBalance());
        accountService.deposit(testAccount, BigDecimal.valueOf(30));
        assertEquals(BigDecimal.valueOf(80),testAccount.getBalance());
    }

    @Test(expected = NegativeDepositException.class)
    public void testNegativeDeposit() throws AccountTakenException, NegativeDepositException {
        accountService.addAccount(new BankAccount("1234", "Justin Korah", BigDecimal.valueOf(50)));
        BankAccount testAccount = accountService.findAccount("1234");
        accountService.deposit(testAccount, BigDecimal.valueOf(-60));
    }
}